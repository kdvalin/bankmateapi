using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using BankMateAPI.Database;
using BankMateAPI.Models;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Xunit;

namespace BankMateAPI.Tests.Integration;
[Collection("VendorTests")]
public class VendorTests : IClassFixture<TestWebApp> {
    private readonly TestWebApp _factory;
    private readonly string API_ENDPOINT = "/vendor";
    private static VendorModel _vendor = new(){ Name = "walmart" };
    private static VendorModel _second_vendor = new(){ Name = "target" };

    public VendorTests(TestWebApp factory)
    {
        _factory = factory;
        var scope = _factory.Services.CreateScope();
        scope.ServiceProvider.GetRequiredService<TxnContext>().Database.EnsureCreated();
    }

    [Fact]
    public async Task EmptyDbShouldReturnNothing()
    {
        var client = _factory.CreateClient();

        var resp = await client.GetAsync(API_ENDPOINT);

        resp.EnsureSuccessStatusCode();
        Assert.Equal("[]", await resp.Content.ReadAsStringAsync());
    }

    [Fact]
    public async Task CreatingValidVendorShouldSucceed() {
        var client = _factory.CreateClient();

        var resp = await CreateVendor(_vendor, client);

        var newVendor = DeserializeVendor(await resp.Content.ReadAsStringAsync());

        _vendor.Id = newVendor.Id;
        Assert.Equal(_vendor, newVendor);
        await Cleanup(client);
    }

    [Fact]
    public async Task CreatingIdenticalVendorsShouldConflict() {
        var client = _factory.CreateClient();
        await CreateVendor(_vendor, client);
        var second = await CreateVendor(_vendor, client, false);

        Assert.Equal(HttpStatusCode.Conflict, second.StatusCode);
        await Cleanup(client);
    }

    [Fact]
    public async Task GettingVendorByIdShouldSucceed() {
        var client = _factory.CreateClient();
        var vendor_created = DeserializeVendor( await (await CreateVendor(_vendor, client)).Content.ReadAsStringAsync());
        var resp = await client.GetAsync($"{API_ENDPOINT}/{vendor_created.Id}");

        resp.EnsureSuccessStatusCode();
        var vendor = DeserializeVendor(await resp.Content.ReadAsStringAsync());
        Assert.Equal(_vendor, vendor);
        await Cleanup(client);
    }

    [Fact]
    public async Task GettingVendorByNonExistentIdShouldFail() {
        var client = _factory.CreateClient();
        var resp = await client.GetAsync($"{API_ENDPOINT}/{Guid.Empty}");
        
        Assert.Equal(HttpStatusCode.NotFound, resp.StatusCode);
    }

    [Fact]
    public async Task DeletingVendorShouldSucceed() {
        var client = _factory.CreateClient();
        var vendor_created = DeserializeVendor( await (await CreateVendor(_vendor, client)).Content.ReadAsStringAsync());
        var resp = await client.DeleteAsync($"{API_ENDPOINT}/{vendor_created.Id}");

        resp.EnsureSuccessStatusCode();
        await Cleanup(client);
    }

    [Fact]
    public async Task DeletingInvalidIdShould404() {
        var client = _factory.CreateClient();

        var resp = await client.DeleteAsync($"{API_ENDPOINT}/{Guid.Empty}");
        Assert.Equal(HttpStatusCode.NotFound, resp.StatusCode);
    }

    [Fact]
    public async Task UpdatingVendorShouldSucceed() {
        var client = _factory.CreateClient();
        var vendor = DeserializeVendor( await (
            await CreateVendor(_vendor, client)
        ).Content.ReadAsStringAsync());

        var resp = await client.PutAsync($"{API_ENDPOINT}/{vendor.Id}", JsonContent.Create(_second_vendor));
        resp.EnsureSuccessStatusCode();
        var new_vendor = DeserializeVendor(await resp.Content.ReadAsStringAsync());

        Assert.Equal(_second_vendor, new_vendor);
        await Cleanup(client);
    }

    [Fact]
    public async Task UpdatingInvalidVendorShould404() {
        var client = _factory.CreateClient();
        var resp = await client.PutAsJsonAsync($"{API_ENDPOINT}/{Guid.Empty}", _second_vendor);
        Assert.Equal(HttpStatusCode.NotFound, resp.StatusCode);
    }

    [Fact]
    public async Task UpdatingVendorToExistingVendorShouldConflict() {
        var client = _factory.CreateClient();
        var vendor = DeserializeVendor(await (
            await CreateVendor(_vendor, client)
        ).Content.ReadAsStringAsync());
        var _ = await CreateVendor(_second_vendor, client);

        var resp = await client.PutAsJsonAsync($"{API_ENDPOINT}/{vendor.Id}", _second_vendor);
        Assert.Equal(HttpStatusCode.Conflict, resp.StatusCode);
        await Cleanup(client);
    }

    private static VendorModel? DeserializeVendor(string input) {
        return JsonConvert.DeserializeObject<VendorModel>(input);
    }

    private async Task<HttpResponseMessage> CreateVendor(VendorModel vendor, HttpClient client, bool checkSuccess = true) {
        var resp = await client.PostAsync(API_ENDPOINT, JsonContent.Create(vendor));

        if(checkSuccess) {
            resp.EnsureSuccessStatusCode();
        }

        return resp;
    }

    private async Task Cleanup(HttpClient client) {
        var resp = await client.GetAsync(API_ENDPOINT);

        var vendors = JsonConvert.DeserializeObject<List<VendorModel>>(await resp.Content.ReadAsStringAsync());
        foreach (var vendor in vendors) {
            await client.DeleteAsync($"{API_ENDPOINT}/{vendor.Id}");
        }
    }
}