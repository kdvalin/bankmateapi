
using System.Linq;
using BankMateAPI.Database;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BankMateAPI.Tests.Integration;

public class TestWebApp : WebApplicationFactory<Program>
{
    private SqliteConnection _sql;

    protected override void ConfigureWebHost(IWebHostBuilder builder)
    {
        _sql = new SqliteConnection("DataSource=:memory:");
        _sql.Open();

        builder.ConfigureTestServices(svcs => {
            svcs.Remove(svcs.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<TxnContext>)));
            svcs.AddDbContext<TxnContext>( opts => opts.UseSqlite(_sql) );
        });
        base.ConfigureWebHost(builder);
    }
}