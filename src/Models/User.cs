using System.ComponentModel.DataAnnotations;

namespace BankMateAPI.Models;

public class UserModel {
    [Key]
    public Guid Id { get; set; }

    public ICollection<Txn> Txns { get; set; } = new List<Txn>();
}