using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace BankMateAPI.Models;

[Index(nameof(Name), IsUnique = true)]
public class VendorModel {
    [Key]
    public Guid Id { get; set; }
    [Required]
    public string Name { get; set; } = "";

    private ICollection<Txn> Txns {get; set;} = new List<Txn>();

    public override bool Equals(object? obj)
    {
        if(obj == null || obj.GetType() != typeof(VendorModel)) {
            return false;
        }
        VendorModel model = (VendorModel)obj;

        return Name == model.Name;
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}
