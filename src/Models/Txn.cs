using System.ComponentModel.DataAnnotations;
using BankMateAPI.Models.Items;

namespace BankMateAPI.Models;

public class Txn {
    [Key]
    public Guid Id { get; private set; }

    [Required]
    public Guid OwnerId {get; set;}
    [Required]
    public Guid VendorId { get; set; }

    public ICollection<TxnItem> Items { get; set; } = new List<TxnItem>();
}