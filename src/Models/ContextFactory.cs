
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace BankMateAPI.Database;

public class TxnContextFactory : IDesignTimeDbContextFactory<TxnContext> {
    public TxnContext CreateDbContext(string[] args) {
        var opts = new DbContextOptionsBuilder<TxnContext>();
        opts.UseSqlite("Data Source = bankmate.db");

        return new TxnContext(opts.Options);
    }
}