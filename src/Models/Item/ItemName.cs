

namespace BankMateAPI.Models.Items;

public class ItemName {
    public Guid Id { get; set; }
    public string Name { get; set; } = "";

    private ICollection<TxnItem> Items { get; set; } = new List<TxnItem>();
}