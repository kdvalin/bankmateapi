using System.ComponentModel.DataAnnotations;

namespace BankMateAPI.Models.Items;

public class TxnItem {
    [Key]
    public Guid Id { get; private set; }
    public Guid ItemNameId { get; set; }
    public Guid TxnId { get; set; }

    public double Price { get; set;}
    public ulong Quantity { get; set; }
}
