using BankMateAPI.Models;
using BankMateAPI.Models.Items;
using Microsoft.EntityFrameworkCore;

namespace BankMateAPI.Database;

public class TxnContext : DbContext {
    public DbSet<ItemName> ItemNames {get; set;}
    public DbSet<TxnItem> Items { get; set; }
    public DbSet<Txn> Txns { get; set; }
    public DbSet<VendorModel> Vendors { get; set; }
    public DbSet<UserModel> Users { get; set; }

#pragma warning disable CS8618 // Non-nullable fields must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public TxnContext(DbContextOptions<TxnContext> contextOptions) : base(contextOptions) {}
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    public UserModel? GetUserById(Guid id) {
        return Users.Find(id);
    }
}