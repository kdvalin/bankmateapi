using System.Net;

namespace BankMateAPI.Exceptions.Vendor;

public class NotFoundException : BankMateException {
    public override HttpStatusCode StatusCode { get; }  = HttpStatusCode.NotFound;
    public NotFoundException(string message) : base(message) {}
}

public class AlreadyExistsException : BankMateException {
    public override HttpStatusCode StatusCode { get; }  = HttpStatusCode.Conflict;
    public AlreadyExistsException(string message) : base(message) {}
}
