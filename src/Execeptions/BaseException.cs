
using System.Net;

namespace BankMateAPI.Exceptions;

public interface IBankMateException {
    public HttpStatusCode StatusCode { get; }
}


public abstract class BankMateException : Exception, IBankMateException {
    public virtual HttpStatusCode StatusCode { get; } = HttpStatusCode.BadRequest;
    public BankMateException(string msg) : base(msg) { }
}