using System.ComponentModel;
using BankMateAPI.Database;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BankMateAPI.Filters;

public abstract class FilterBase : ActionFilterAttribute {
    public TxnContext GetTxnContext(ActionExecutingContext context) {
        return context.HttpContext.RequestServices.GetService<TxnContext>();
    }
    public T GetPathVariable<T>(ActionExecutingContext context, string key) {
        var value = context.HttpContext.Request.RouteValues[key].ToString();

        if( value is T t) {
            return t;
        }

        var converter = TypeDescriptor.GetConverter(typeof(T));

        return (T) converter.ConvertFromString(value);
    }
}