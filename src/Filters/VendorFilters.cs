using BankMateAPI.Exceptions.Vendor;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BankMateAPI.Filters.Vendor;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
public class VendorExists : FilterBase {
    private string pathVariable;
    public VendorExists(string key = "id") {
        pathVariable = key;
    }

    public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) {
        var _dbContext = GetTxnContext(context);
        var vendor_id = GetPathVariable<Guid>(context, pathVariable);

        if(_dbContext.Vendors.Find(vendor_id) == null) {
            throw new NotFoundException($"Vendor with id \"{vendor_id}\" doesn't exist!");
        }

        await next();
    }
}
