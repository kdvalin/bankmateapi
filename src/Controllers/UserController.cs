using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

using BankMateAPI.Database;
using BankMateAPI.Models;

namespace BankMateAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : Controller {
    private readonly ILogger<VendorController> _logger;
    private TxnContext _db;

    public UserController(ILogger<VendorController> logger, TxnContext db) {
        _logger = logger;
        _db = db;
    }

    [Authorize]
    [HttpGet("me")]
    public async Task<UserModel> GetAuthenticatedUser() {
        var id = Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
        var user = _db.GetUserById(id);

        if(user != null) {
            return user;
        }
        user = new UserModel{
            Id = id
        };

        _db.Users.Add(user);
        await _db.SaveChangesAsync();

        return user;
    }
}