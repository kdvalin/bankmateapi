using Microsoft.AspNetCore.Mvc;

using BankMateAPI.Database;
using BankMateAPI.Models;
using BankMateAPI.Filters.Vendor;
using BankMateAPI.Exceptions.Vendor;

namespace BankMateAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class VendorController {
    private readonly ILogger<VendorController> _logger;
    private TxnContext _db;

    public VendorController(ILogger<VendorController> logger, TxnContext db) {
        _logger = logger;
        _db = db;
    }

    [HttpGet("")]
    public List<VendorModel> GetVendorModels() {
        return _db.Vendors.Select(x => x).ToList();
    }

    [HttpGet("{id}")]
    [VendorExists]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult<VendorModel> GetVendorModel(Guid id) {
        var result = GetVendorById(id);

        if(result == null) {
            return new NotFoundResult();
        }
        return result;
    }

    [HttpPost("")]
    public ActionResult<VendorModel> CreateVendorModel(VendorModel input) {
        input.Id = new Guid();
        input.Name = input.Name.ToLower();

        if(GetVendorByName(input.Name) != null) {
            throw new AlreadyExistsException($"A vendor already exists with the name \"{input.Name}\"");
        }

        _db.Vendors.Add(input);
        _db.SaveChanges();

        return new CreatedResult($"/vendor/{input.Id}", input);
    }

    [HttpPut("{id}")]
    [VendorExists]
    public ActionResult<VendorModel> UpdateVendorModel(Guid id, VendorModel input) {
        var db_model = GetVendorById(id);

        if(db_model == null) {
            return new NotFoundResult();
        }
        if(GetVendorByName(input.Name) != null) {
            throw new AlreadyExistsException($"Vendor with name \"{input.Name}\" already exists!");
        }

        db_model.Name = input.Name;
        _db.SaveChanges();
        return db_model;
    }

    [HttpDelete("{id}")]
    [VendorExists]
    public ActionResult DeleteVendorModel(Guid id) {
        var db_model = GetVendorById(id);

        if(db_model == null) {
            return new NotFoundResult();
        }

        _db.Vendors.Remove(db_model);
        _db.SaveChanges();
        return new NoContentResult();
    }

    private VendorModel? GetVendorById(Guid id) {
        return _db.Vendors.Find(id);
    }

    private VendorModel? GetVendorByName(string name) {
        return _db.Vendors.SingleOrDefault(x => x.Name.Equals(name));
    }
}