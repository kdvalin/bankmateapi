using Newtonsoft.Json;
using System.Net;
using System.Net.Mime;

using BankMateAPI.Exceptions;

namespace BankMateAPI.Middleware;

public class ErrorHandlerMiddleware {
    private readonly RequestDelegate request;
    private readonly ILogger logger;

    public ErrorHandlerMiddleware(RequestDelegate request, ILogger<ErrorHandlerMiddleware> logger)
    {
        this.request = request;
        this.logger = logger;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await request(context);
        }
        catch (Exception exception)
        {
            await WriteExceptionToResponse(exception, context);
        }
    }

    private Task WriteExceptionToResponse(Exception exception, HttpContext context)
    {
        logger.LogError($"Unhandled Exception: {exception.Message}");

        var result = JsonConvert.SerializeObject(new { error = exception.Message });
        context.Response.ContentType = MediaTypeNames.Application.Json;
        context.Response.StatusCode = GetStatusCode(exception);
        return context.Response.WriteAsync(result);
    }

    private int GetStatusCode(Exception exception)
    {
        var code = HttpStatusCode.InternalServerError;

        if (exception is BankMateException e)
        {
            code = e.StatusCode;
        }

        return (int)code;
    }
}