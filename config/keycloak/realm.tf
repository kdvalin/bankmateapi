resource "keycloak_realm" "BankmateRealm" {
    realm = "BankMate"
    enabled = true
    display_name = "BankMate"

    login_theme = "base"

    registration_allowed = true
    registration_email_as_username = true

    ssl_required = "external"
}
