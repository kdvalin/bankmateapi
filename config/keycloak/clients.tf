resource "keycloak_openid_client" "DevClient" {
  realm_id = keycloak_realm.BankmateRealm.id
  client_id = "DevClient"
  name = "Development Client"
  enabled = true

  access_type = "PUBLIC"
}